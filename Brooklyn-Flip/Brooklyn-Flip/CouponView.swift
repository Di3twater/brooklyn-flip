//
//  CouponView.swift
//  Brooklyn-Flip
//
//  Created by JoNATHAN GREENE on 6/15/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit

class CouponView: UIViewController {

    @IBOutlet weak var QRcode: UIImageView!
    @IBOutlet weak var couponCode: UILabel!
    @IBOutlet weak var couponName: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    
    @IBOutlet weak var couponImage: UIImageView!
    var qrcodeImage:CIImage!
    
    var currentBuisness:Buisness = Buisness(myUser:"",myImage: UIImage(), myPlace: "", myCoupon: "", myOg: "", myAfter: "", myBought: "", myCode: "", myRemain: "",myLat: Double(),myLong:Double(), myID:String())
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        couponImage.image = currentBuisness.image
        couponName.text = currentBuisness.CouponName
        placeLabel.text = currentBuisness.Place

        var myCode = currentBuisness.code
        
        let data = myCode.dataUsingEncoding(NSISOLatin1StringEncoding, allowLossyConversion: false)
        
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter.setValue(data, forKey: "inputMessage")
        filter.setValue("Q", forKey: "inputCorrectionLevel")
        
        qrcodeImage = filter.outputImage
        
        let scaleX = QRcode.frame.size.width / qrcodeImage.extent().size.width
        let scaleY = QRcode.frame.size.height / qrcodeImage.extent().size.height
        
        
        let transformedImage = qrcodeImage.imageByApplyingTransform(CGAffineTransformMakeScale(scaleX, scaleY))
        
        QRcode.image = UIImage(CIImage: transformedImage)
        
        couponCode.text = myCode
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
