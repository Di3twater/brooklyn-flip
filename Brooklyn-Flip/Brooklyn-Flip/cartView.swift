//
//  cartView.swift
//  Brooklyn-Flip
//
//  Created by JoNATHAN GREENE on 6/17/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit
import Parse

class cartView: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var boughtArray:[Buisness] = []
    
    var currentUser = PFUser.currentUser()
    
    @IBOutlet weak var myTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        var query = PFQuery(className: currentUser!.objectId! )
        
        var objects = query.findObjects()
        if let obj = objects {
            
            for object in obj {
                
                let theUser = currentUser?.objectId
                let thePlace = object.valueForKey("Place") as! String
                let theCoupon = object.valueForKey("CouponName") as! String
                let theAfter = object.valueForKey("After") as! String
                let theCode = object.valueForKey("CouponCode") as! String
                if let theImage = object.valueForKey("Image") as? PFFile{
                    
                    
                    theImage.getDataInBackgroundWithBlock {
                        (imageData: NSData?, error: NSError?) -> Void in
                        if error == nil {
                            if let imageData = imageData {
                                let image = UIImage(data:imageData)
                                var theData:Buisness = Buisness(myUser:theUser!,myImage: image!, myPlace: thePlace, myCoupon: theCoupon, myOg: "", myAfter: theAfter, myBought: "", myCode: theCode, myRemain: "",myLat: Double(),myLong:Double(), myID:String())
                                
                                self.boughtArray.append(theData)
                            }
                        }
                        
                        
                        self.myTable.reloadData()
                    }
                }
                
            }
            
            //END
            //  Edited by Jonathan Green on 5/3/15.
        }
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        
        return boughtArray.count
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: cartCell = tableView.dequeueReusableCellWithIdentifier("MyPurchaseCell")
            as! cartCell
        
        let currentBuisness:Buisness = boughtArray[indexPath.row]
        
        cell.CouponName.text = currentBuisness.CouponName
        cell.after.text = currentBuisness.after
        
        cell.datePurchased.text = ""
        
        return cell
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        var indexPath:NSIndexPath? = myTable.indexPathForSelectedRow()
        
        let currentBuisness:Buisness = boughtArray[indexPath!.row]
        
        if (segue.identifier == "myCode"){
            
            var couponView:CouponView = segue.destinationViewController as! CouponView
            
            couponView.currentBuisness = currentBuisness
            
            
        }
        
        
    }

}
