//
//  wishListViewController.swift
//  Brooklyn-Flip
//
//  Created by JoNATHAN GREENE on 6/18/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit
import Parse

class wishListViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    var wishListArray:[Buisness] = []
    
    var currentUser = PFUser.currentUser()

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        var query = PFQuery(className: "\(currentUser!.objectId!)List" )
        
        var objects = query.findObjects()
        if let obj = objects {
            
            for object in obj {
                
                let rowID = object.valueForKey("objectId") as! String
                let theUser = currentUser?.objectId
                let thePlace = object.valueForKey("Place") as! String
                let theCoupon = object.valueForKey("CouponName") as! String
                let theAfter = object.valueForKey("After") as! String
                let theCode = object.valueForKey("CouponCode") as! String
                if let theImage = object.valueForKey("Image") as? PFFile{
                    
                    
                    theImage.getDataInBackgroundWithBlock {
                        (imageData: NSData?, error: NSError?) -> Void in
                        if error == nil {
                            if let imageData = imageData {
                                let image = UIImage(data:imageData)
                                var theData:Buisness = Buisness(myUser:theUser!,myImage: image!, myPlace: thePlace, myCoupon: theCoupon, myOg: "", myAfter: theAfter, myBought: "", myCode: theCode, myRemain: "",myLat: Double(),myLong:Double(), myID: rowID)
                                
                                self.wishListArray.append(theData)
                            }
                        }
                        
                        
                        self.tableView.reloadData()
                    }
                }
                
            }
            
            //END
            //  Edited by Jonathan Green on 5/3/15.
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        
        return wishListArray.count
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: cartCell = tableView.dequeueReusableCellWithIdentifier("MyPurchaseCell")
            as! cartCell
        
        let currentBuisness:Buisness = wishListArray[indexPath.row]
        
        cell.CouponName.text = currentBuisness.CouponName
        cell.after.text = currentBuisness.after
        
        cell.datePurchased.text = ""
        
        return cell
        
    }
    
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
        
        
        let currentBuisness:Buisness = wishListArray[indexPath.row]
       
        var Buy = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Buy" , handler: { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in

            
            var query = PFQuery(className:currentBuisness.user)
            
            var myImage = NSData(data: UIImagePNGRepresentation(currentBuisness.image))
            
            let dataFile = NSData(data: myImage)
            let file = PFFile(data: dataFile)
            
            query.findObjectsInBackgroundWithBlock {
                (objects: [AnyObject]?, error: NSError?) -> Void in
                
                if error == nil {
                    
                    var bought = PFObject(className:currentBuisness.user)
                    
                    bought["Image"] = file
                    bought["Place"] = currentBuisness.Place
                    bought["CouponName"] = currentBuisness.CouponName
                    bought["After"] = currentBuisness.after
                    bought["CouponCode"] = currentBuisness.code
                    bought.saveInBackgroundWithBlock {
                        (success: Bool, error: NSError?) -> Void in
                        if (success) {
                            self.performSegueWithIdentifier("bought", sender: nil)
                            self.wishListArray.removeAtIndex(indexPath.row)
                            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                            
                            var query = PFQuery(className: "\(self.currentUser!.objectId!)List")
                            
                            query.getObjectInBackgroundWithId("\(currentBuisness.objectId)") {
                                (object: PFObject?, error: NSError?) -> Void in
                                if error == nil {
                                    
                                    object?.deleteInBackground()
                                    
                                    println("deleted")
                                    
                                } else {
                                    println("error")
                                }
                            }
                            
                        } else {
                            // There was a problem, check error.description
                        }
                    }
                    
                    
                    
                    
                } else {
                    
                    println("error")
                    
                }
                
                
            }
            
           })
        
        
        var Remove = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Remove" , handler: { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in
            
            let currentBuisness:Buisness = self.wishListArray[indexPath.row]
            println("remove")
            
            self.wishListArray.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            
            var query = PFQuery(className: "\(self.currentUser!.objectId!)List")
            
            query.getObjectInBackgroundWithId("\(currentBuisness.objectId)") {
                (object: PFObject?, error: NSError?) -> Void in
                if error == nil {
                    
                    object?.deleteInBackground()
                    
                    println("deleted")
                    
                } else {
                    println("error")
                }
            }
            
            })
        
            return [Buy,Remove]
    }
}
