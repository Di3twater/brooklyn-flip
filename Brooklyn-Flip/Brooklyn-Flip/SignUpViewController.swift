//
//  SignUpViewController.swift
//  Brooklyn-Flip
//
//  Created by JoNATHAN GREENE on 6/4/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit
import Parse

class SignUpViewController: UIViewController {

    @IBOutlet weak var firstNameLbl: UITextField!
    @IBOutlet weak var lastNameLbl: UITextField!
    @IBOutlet weak var emailLbl: UITextField!
    @IBOutlet weak var passLbl: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func signUp(sender: AnyObject) {
        
        if (firstNameLbl.text != nil) && (lastNameLbl.text != nil) && (emailLbl.text != nil) && (passLbl.text != nil) {
            
            var user = PFUser()
            user.username = emailLbl.text
            user.password = passLbl.text
            user.email = emailLbl.text
            user["firstName"] = firstNameLbl.text
            user["lastName"] = lastNameLbl.text
            
            user.signUpInBackgroundWithBlock {
                (succeeded: Bool, error: NSError?) -> Void in
                if error != nil {
                    
                    println("SignUp Error")
                    
                } else {
                    self.performSegueWithIdentifier("goTo", sender: self)
                    println("Regestered")
                }
            }
            
            
        }else{
            
            println("one or more fields are missing")
        }
        
    }
   
}
