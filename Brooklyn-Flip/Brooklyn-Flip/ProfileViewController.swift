//
//  ProfileViewController.swift
//  Brooklyn-Flip
//
//  Created by JoNATHAN GREENE on 6/4/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit
import Parse

class ProfileViewController: UIViewController {

    @IBOutlet weak var mainFirst: UILabel!
    @IBOutlet weak var mainEmail: UILabel!
    @IBOutlet weak var first: UILabel!
    @IBOutlet weak var last: UILabel!
    @IBOutlet weak var email: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        var currentUser = PFUser.currentUser()
        let firstName = currentUser?.valueForKey("firstName") as! String
        let lastName = currentUser?.valueForKey("lastName") as! String
        
        mainEmail.text = currentUser?.email
        mainFirst.text = "\(firstName) \(lastName)"
        email.text = currentUser?.email
        first.text = firstName
        last.text = lastName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func signOut(sender: AnyObject) {
        
        PFUser.logOut()
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

}
