//
//  BuisnessClass.swift
//  Brooklyn-Flip
//
//  Created by JoNATHAN GREENE on 6/15/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit


class Buisness {
    var user = ""
    var image = UIImage()
    var Place = ""
    var CouponName = ""
    var Og = ""
    var after = ""
    var bought = ""
    var code = ""
    var remain = ""
    var lat = Double()
    var long = Double()
    var objectId = ""
    
    init(myUser:String,myImage:UIImage, myPlace:String, myCoupon:String, myOg:String, myAfter:String, myBought:String, myCode:String, myRemain:String,myLat:Double,myLong:Double, myID:String){
        
        user = myUser
        image = myImage
        Place = myPlace
        CouponName = myCoupon
        Og = myOg
        after = myAfter
        bought = myBought
        code = myCode
        remain = myRemain
        lat = myLat
        long = myLong
        objectId = myID
        
    }
}
