//
//  EditViewController.swift
//  Brooklyn-Flip
//
//  Created by JoNATHAN GREENE on 6/24/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit
import Parse

class EditViewController: UIViewController {
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    
    var currentUser = PFUser.currentUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func Save(sender: AnyObject) {
        
        currentUser?.setObject(firstName.text, forKey: "firstName")
        currentUser?.setObject(lastName.text, forKey: "lastName")
        currentUser?.setObject(email.text, forKey: "email")
        currentUser?.username = email.text
        
        currentUser?.save()
        
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: {})
        println("Save")
        
    }

    @IBAction func Cancle(sender: AnyObject) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: {})
        println("cancel")
        
           }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
