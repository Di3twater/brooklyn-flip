//
//  dailyCell.swift
//  Brooklyn-Flip
//
//  Created by JoNATHAN GREENE on 6/2/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit

class dailyCell: UITableViewCell {

    @IBOutlet weak var thisOriginal: UILabel!
    @IBOutlet weak var thisAfter: UILabel!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var thisImage: UIImageView!
    @IBOutlet weak var bought: UILabel!
    @IBOutlet weak var thisPlace: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
