//
//  MapView.swift
//  Brooklyn-Flip
//
//  Created by JoNATHAN GREENE on 6/11/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit
import MapKit
import AddressBook

class MapView: UIViewController {

    @IBOutlet weak var myMap: MKMapView!
    
    
    let regionRadius: CLLocationDistance = 1000
    
    let annotation = MKPointAnnotation()
   
    var currentBuisness:Buisness = Buisness(myUser:"",myImage: UIImage(), myPlace: "", myCoupon: "", myOg: "", myAfter: "", myBought: "", myCode: "", myRemain: "",myLat: Double(),myLong:Double(), myID:String())
    
    override func viewWillAppear(animated: Bool) {
        let initialLocation = CLLocation(latitude: currentBuisness.lat, longitude: currentBuisness.long)
        
        var location = CLLocationCoordinate2D(latitude: currentBuisness.lat, longitude: currentBuisness.long)
        
        annotation.coordinate = location
        
        centerMapOnLocation(initialLocation)
        
        myMap.addAnnotation(annotation)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        myMap.setRegion(coordinateRegion, animated: true)
    }
    @IBAction func Directions(sender: AnyObject) {
        
        let addressDictionary = [String(kABPersonAddressStreetKey): "test"]
        
        var location = CLLocationCoordinate2D(latitude: currentBuisness.lat, longitude: currentBuisness.long)
        
        let myPlaceMark = MKPlacemark(coordinate: location, addressDictionary: addressDictionary)
        
        var mapItem = MKMapItem(placemark: myPlaceMark)
        
        mapItem.name = "The way I want to go"
        
        //You could also choose: MKLaunchOptionsDirectionsModeWalking
        var launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
        
        mapItem.openInMapsWithLaunchOptions(launchOptions)
    }

}
