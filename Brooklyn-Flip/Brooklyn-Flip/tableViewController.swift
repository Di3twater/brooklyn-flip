//
//  tableViewController.swift
//  Brooklyn-Flip
//
//  Created by JoNATHAN GREENE on 6/2/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit
import Parse

class tableViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
  
    var myArray:[Buisness] = []
    
    var currentUser = PFUser.currentUser()
    
    
    @IBOutlet weak var myTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        var query = PFQuery(className: "Buiesness")
        
        var objects = query.findObjects()
        if let obj = objects {
            
            for object in obj {
                
                let theUser = currentUser?.objectId
                let thePlace = object.valueForKey("Place") as! String
                let theCoupon = object.valueForKey("CouponName") as! String
                let theOriginal = object.valueForKey("Original") as! String
                let theAfter = object.valueForKey("After") as! String
                let theBought = object.valueForKey("Bought") as! String
                let theCode = object.valueForKey("CouponCode") as! String
                let theRemain = object.valueForKey("remaining") as! String
                let theLat = object.valueForKey("Lat") as! Double
                let theLong = object.valueForKey("Long") as! Double
                
                if let theImage = object.valueForKey("Image") as? PFFile{
                    
                    
                    theImage.getDataInBackgroundWithBlock {
                        (imageData: NSData?, error: NSError?) -> Void in
                        if error == nil {
                            if let imageData = imageData {
                                let image = UIImage(data:imageData)
                                var theData:Buisness = Buisness(myUser:theUser!,myImage: image!, myPlace: thePlace, myCoupon: theCoupon, myOg: theOriginal, myAfter: theAfter, myBought: theBought, myCode: theCode, myRemain: theRemain, myLat: theLat, myLong:theLong, myID:String())
                                
                                self.myArray.append(theData)
                            }
                        }
                        
                        
                        self.myTable.reloadData()
                    }
                }
                
            }
            
        }

        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.hidesBackButton = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        
        return myArray.count
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: dailyCell = tableView.dequeueReusableCellWithIdentifier("DailyDealsCell")
            as! dailyCell
        
        let currentBuisness:Buisness = myArray[indexPath.row]
        
        cell.bought.text = currentBuisness.bought
        cell.thisAfter.text = currentBuisness.after
        cell.thisImage.image = currentBuisness.image
        cell.thisOriginal.text = currentBuisness.Og
        cell.Name.text = currentBuisness.CouponName
        cell.thisPlace.text = currentBuisness.Place
        
        return cell
        
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "Detail"){
            
            var detailView:DetailView = segue.destinationViewController as! DetailView
            
            var indexPath:NSIndexPath? = myTable.indexPathForSelectedRow()
            
            let currentBuisness:Buisness = myArray[indexPath!.row]
            detailView.currentBuisness = currentBuisness
            
        }
        
       
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    
}
