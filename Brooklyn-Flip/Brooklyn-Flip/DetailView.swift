//
//  DetailView.swift
//  Brooklyn-Flip
//
//  Created by JoNATHAN GREENE on 6/15/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit
import Parse

class DetailView: UIViewController {
    
    @IBOutlet weak var detailImage: UIImageView!
    
    @IBOutlet weak var detailCoupon: UILabel!
    
    @IBOutlet weak var detailPlace: UILabel!
    
    @IBOutlet weak var detailAfter: UILabel!
    
    @IBOutlet weak var detailOG: UILabel!
    
    @IBOutlet weak var remain: UILabel!
    
    @IBOutlet weak var savings: UILabel!
    
    @IBOutlet weak var desc: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    var currentBuisness:Buisness = Buisness(myUser: "", myImage: UIImage(), myPlace: "", myCoupon: "", myOg: "", myAfter: "", myBought: "", myCode: "", myRemain: "", myLat: Double(), myLong: Double(), myID:String())

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        detailPlace.text = currentBuisness.Place
        detailOG.text = currentBuisness.Og
        detailCoupon.text = currentBuisness.CouponName
        detailAfter.text = currentBuisness.after
        detailImage.image = currentBuisness.image
        remain.text = currentBuisness.remain
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "Buy"){
            
            var couponView:CouponView = segue.destinationViewController as! CouponView
            
            couponView.currentBuisness = currentBuisness
            
            var query = PFQuery(className:currentBuisness.user)
            
            var myImage = NSData(data: UIImagePNGRepresentation(currentBuisness.image))
            
            let dataFile = NSData(data: myImage)
            let file = PFFile(data: dataFile)
            
            query.findObjectsInBackgroundWithBlock {
                (objects: [AnyObject]?, error: NSError?) -> Void in
                
                if error == nil {
                    
                    var bought = PFObject(className:self.currentBuisness.user)
                    
                    bought["Image"] = file
                    bought["Place"] = self.currentBuisness.Place
                    bought["CouponName"] = self.currentBuisness.CouponName
                    bought["After"] = self.currentBuisness.after
                    bought["CouponCode"] = self.currentBuisness.code
                    bought.saveInBackgroundWithBlock {
                        (success: Bool, error: NSError?) -> Void in
                        if (success) {
                            // The object has been saved.
                        } else {
                            // There was a problem, check error.description
                        }
                    }
                    
                    
                 
                    
                }else if (segue.identifier == "heart"){
                    
                    var wishView:wishListViewController = segue.destinationViewController as! wishListViewController
                    
                   /*wishView.currentBuisness = self.currentBuisness*/
                    
                    
                }else {
                    
                    println("error")
                    
                }

                
            }
            
            
            
        }else if (segue.identifier == "myMap"){
            
            var myMapView:MapView = segue.destinationViewController as! MapView
            
            myMapView.currentBuisness = currentBuisness
        }
        
        
    }

    
        
    

   
    @IBAction func addWish(sender: AnyObject) {
        
        var query = PFQuery(className:"\(currentBuisness.user)List")
        
        var myImage = NSData(data: UIImagePNGRepresentation(currentBuisness.image))
        
        let dataFile = NSData(data: myImage)
        let file = PFFile(data: dataFile)
        
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                
                var bought = PFObject(className:"\(self.currentBuisness.user)List")
                
                bought["Image"] = file
                bought["Place"] = self.currentBuisness.Place
                bought["CouponName"] = self.currentBuisness.CouponName
                bought["After"] = self.currentBuisness.after
                bought["CouponCode"] = self.currentBuisness.code
                bought.saveInBackgroundWithBlock {
                    (success: Bool, error: NSError?) -> Void in
                    if (success) {
                        
                        println(self.currentBuisness.Place)
                        println("added to list")
                    } else {
                        // There was a problem, check error.description
                    }
                }
                
                
                
                
            } else {
                
                println("error")
                
            }
            
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
