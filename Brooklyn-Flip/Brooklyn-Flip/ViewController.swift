//
//  ViewController.swift
//  Brooklyn-Flip
//
//  Created by JoNATHAN GREENE on 6/2/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {

    @IBOutlet weak var Email: UITextField!
    @IBOutlet weak var passWord: UITextField!
    
    @IBOutlet weak var MyImage: SpringImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.hidesBackButton = true
        Email.text = nil
        passWord.text = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func myLogin(sender: AnyObject) {
        
        PFUser.logInWithUsernameInBackground(Email.text, password:passWord.text) {
            (user: PFUser?, error: NSError?) -> Void in
            if user != nil {
               println("Login Succesfull")
                self.performSegueWithIdentifier("Login", sender: self)
            
            } else {
                
                println("The login failed. Check error to see why.")
                
            }
        }
    }
    
    
    @IBAction func FacebookLog(sender: AnyObject) {
        
        
        
        
    }
    
    @IBAction func twiiter(sender: AnyObject) {
        
        PFTwitterUtils.logInWithBlock {
            (user: PFUser?, error: NSError?) -> Void in
            if let user = user {
                if user.isNew {
                    self.performSegueWithIdentifier("Login", sender: self)
                    PFUser.currentUser()?.username = PFTwitterUtils.twitter()?.screenName
                    PFUser.currentUser()?.email = "change me"
                    user["firstName"] = "change me"
                    user["lastName"] = "change me"
                    println("User signed up and logged in with Twitter!")
                } else {
                    self.performSegueWithIdentifier("Login", sender: self)
                    println("User logged in with Twitter!")
                }
            } else {
                println("Uh oh. The user cancelled the Twitter login.")
            }
        }
    }
}

